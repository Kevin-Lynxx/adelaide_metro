import requests
import csv
import os
import datetime
from settings import INPUT_DATA, RAW_DATA
from utils.utilities import save_json


def load_stops(file):
    stops_list = []
    with open(file) as f:
        f_csv = csv.reader(f)
        for row in f_csv:
            stops_list.append(row[0])
    return stops_list


def stop_monitoring(lines, stops):
    print '\nYou have now entered stop_monitoring'
    for line in lines:
        for stop in stops:
            requesttime = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
            r = requests.get('http://realtime.adelaidemetro.com.au/SiriWebServiceSAVM/SiriStopMonitoring.svc/json/SM?MonitoringRef={}&LineRef={}'.format(str(stop), str(line)))
            filename = 'stop_monitoring_{}_{}_{}'.format(str(line), str(stop), requesttime)
            save_json(r.json(), filename, RAW_DATA)

if __name__ == '__main__':
    lines = [254]
    stops = load_stops(os.path.join(INPUT_DATA, 'Stops.csv'))
    stop_monitoring(lines, stops)